Spring PoC Install
===

```
vagrant up
vagrant ssh
cd /vagrant
mvn spring-boot:run
```

API
===
http://192.168.4.4/media

RabbitMq
===

http://192.168.4.4:15672
login: guest
password: guest