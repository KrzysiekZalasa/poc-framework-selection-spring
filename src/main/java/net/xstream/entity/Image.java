package net.xstream.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Java domain object representing image
 *
 * @author Artur Borodziej
 */
@Entity
@Table(name = "image")
public class Image {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * Image src
     */
    @Column(name = "src", nullable = false, length = 255)
    private String src;

    /**
     * Image width in pixels
     */
    @Column(name = "width", nullable = false)
    private int width;

    /**
     * Image height in pixels
     */
    @Column(name = "height", nullable = false)
    private int height;

    /**
     * Media associated with the image
     */
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "image_media_id", nullable = false)
    private Media media;

    public Image() {
    }

    public Image(String src, int width, int height) {
        this.src = src;
        this.width = width;
        this.height = height;
    }

    public Image(String src, int width, int height, Media media) {
        this.src = src;
        this.width = width;
        this.height = height;
        this.media = media;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }

    @Override
    public String toString() {
        return "Image{" +
                "id=" + id +
                ", src='" + src + '\'' +
                ", width=" + width +
                ", height=" + height +
                ", media=" + media +
                '}';
    }
}
