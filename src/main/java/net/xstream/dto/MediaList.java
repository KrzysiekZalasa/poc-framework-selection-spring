package net.xstream.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import net.xstream.entity.Media;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by artur.borodziej on 17.06.15.
 */
public class MediaList extends ResourceSupport {

    private List<ListedMedia> media;

    private int limit;

    private int offset;

    private long total;

    @JsonCreator
    public MediaList(Page<Media> media, int limit, int offset, long total) {
        ArrayList<ListedMedia> listedMedia = new ArrayList<>();
        for (Media medium : media) {
            listedMedia.add(new ListedMedia(medium));
        }
        this.media = listedMedia;
        this.limit = limit;
        this.offset = offset;
        this.total = total;
    }

    public List<ListedMedia> getMedia() {
        return media;
    }

    public int getLimit() {
        return limit;
    }

    public int getOffset() {
        return offset;
    }

    public long getTotal() {
        return total;
    }
}
