package net.xstream.dto;

import net.xstream.entity.Image;
import net.xstream.entity.Media;

import java.util.Set;

/**
 * Created by artur.borodziej on 17.06.15.
 */
public class ListedMedia {

    private Long id;

    private String title;

    private String description;

    private Set<Image> images;

    public ListedMedia() {
    }

    public ListedMedia(Media media) {
        id = media.getId();
        title = media.getTitle();
        description = media.getDescription();
        images = media.getImages();
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Set<Image> getImages() {
        return images;
    }
}
