package net.xstream.controller;

import net.xstream.dto.MediaList;
import net.xstream.entity.Media;
import net.xstream.manager.MediaManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by artur.borodziej on 17.06.15.
 */
@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/media")
public class MediaController {

    private final MediaManager manager;

    @Autowired
    public MediaController(MediaManager manager) {
        this.manager = manager;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity create(@RequestBody @Valid Media media) {
        manager.save(media);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<MediaList> read(
            @RequestParam(required = false, defaultValue = "8") int limit,
            @RequestParam(required = false, defaultValue = "0") int offset,
            @RequestParam(required = false, defaultValue = "asc") String order,
            @RequestParam(required = false, defaultValue = "id") String sort
    ) {
        Page<Media> media = manager.find(offset, limit, sort, order);
        MediaList mediaList = new MediaList(media, limit, offset, manager.count());
        mediaList.add(linkTo(methodOn(MediaController.class).read(limit, offset, order, sort)).withSelfRel());
        return new ResponseEntity<MediaList>(mediaList, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Media findOne(@PathVariable Long id) {
        return manager.findOne(id);
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity update(@PathVariable Long id, @RequestBody @Valid Media updated) {
        Media media = manager.findOne(id);
        if (media == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        manager.merge(media, updated);
        manager.save(media);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable long id) {
        if (manager.findOne(id) == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        manager.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
