package net.xstream.djament_import.queue;

import net.xstream.djament_import.DjamentImporter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by artur.borodziej on 17.06.15.
 */
@Service
public class Receiver {

    private DjamentImporter djamentImporter;

    @Autowired
    public Receiver(DjamentImporter djamentImporter) {
        this.djamentImporter = djamentImporter;
    }

    public void receive(String message) {
        djamentImporter.importFromUrl(message);
    }
}
