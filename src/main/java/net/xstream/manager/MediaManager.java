package net.xstream.manager;

import net.xstream.entity.Media;
import net.xstream.repository.MediaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import static org.springframework.data.domain.Sort.Direction.valueOf;

/**
 * Created by artur.borodziej on 18.06.15.
 */
@Service
public class MediaManager {

    private MediaRepository repository;

    @Autowired
    public MediaManager(MediaRepository repository) {
        this.repository = repository;
    }

    public void save(Media media) {
        repository.save(media);
    }

    public Page<Media> find(int offset, int limit, String sort, String order) {
        int page = offset / limit;
        return repository.findAll(
                new PageRequest(
                        page, limit, new Sort(
                            new Order(valueOf(order.toUpperCase()), sort)
                        )
                )
        );
    }

    public long count() {
        return repository.count();
    }

    public Media findOne(Long id) {
        return repository.findOne(id);
    }

    public void delete(Long id) {
        repository.delete(id);
    }

    public void merge(Media media, Media updated) {
        if (updated.getTitle() != null) {
            media.setTitle(updated.getTitle());
        }
        if (updated.getDescription() != null) {
            media.setDescription(updated.getDescription());
        }
    }
}
