package net.xstream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocFrameworkSelectionSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(PocFrameworkSelectionSpringApplication.class, args);
    }
}
