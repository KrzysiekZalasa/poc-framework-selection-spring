package net.xstream.repository;

import net.xstream.entity.Media;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Repository class for <code>Media<code/>s domain objects
 *
 * @author Artur Borodziej
 */
public interface MediaRepository extends PagingAndSortingRepository<Media, Long> {


    Media findOneByExternalId(String externalId);

    Long countById(Long id);

}
