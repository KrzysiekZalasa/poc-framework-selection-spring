package net.xstream.repository;

import net.xstream.entity.Stream;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository class for <code>Stream<code/>s domain objects
 *
 * @author Artur Borodziej
 */
public interface StreamRepository extends CrudRepository<Stream, Long> {
}
